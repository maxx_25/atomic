REQUEST_STANDARD_ATTRS = [
    'message',
    'attr_addr_street',
    'attr_addr_ward',
    'attr_addr_district',
    'attr_addr_city',
    'attr_area',
    'attr_price',
    'attr_interior_floor',
    'attr_interior_room',
    'attr_orientation',
    'attr_project',
    'attr_legal',
    'location_lat',
    'location_lng'
]

RCODE_DONE    = 0
RCODE_IPASS   = 1
RCODE_IDATTYP = 2
RCODE_IDATFMR = 3