from flask import Blueprint, jsonify, request
import json

from app.modules.make_up.make_up import make_up
from app.modules.PostInfo import PostInfo
from app.misc import authenticate, reply_client
import app.resources as resources

mod = Blueprint('api', __name__)

@mod.route('/api/make_up', methods=['POST'])
def trigger_make_up():
    # kiểm tra password
    if authenticate(request) is False:
        return reply_client(resources.RCODE_IPASS, None)

    # kiểm tra xem request gửi lên data có đang ở dạng JSON không
    if request.is_json:
        request_data = json.loads(request.data)

        # print(request_data.keys())

        if request_data['message'] is None:
            return reply_client(resources.RCODE_IDATTYP, None)
            
        # kiểm tra JSON có đủ tất cả các trường như đã quy ước hay không
        if list(request_data.keys()) == resources.REQUEST_STANDARD_ATTRS:
            post_info = PostInfo()
            post_info.set_info_all(request_data)

            make_up(post_info)
            return reply_client(resources.RCODE_DONE, post_info.get_info_for_pushing())
        else:
            # return reply_client(0, make_up(request.get_json()))
            return reply_client(resources.RCODE_IDATFMR, None)
    else:
        return reply_client(resources.RCODE_IDATTYP, None)


@mod.route('/api/predict', methods=['POST'])
def trigger_predict():
    pass

    
@mod.route('/api/visualize', methods=['POST'])
def trigger_visualize():
    pass