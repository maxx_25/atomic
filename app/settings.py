import urllib.parse
import logging
import os

# try:
#     ENV = os.environ['ENV']
# except KeyError:
#     logging.error("Bad ENV argument.")
#     exit(1)

ENV = "PROD"

# resources cho Google Geolocation API
KEY_GOOGLEAPI  = '&key=AIzaSyBrAY6Ms6kNuWAgjnTz58gMpsgtMliyv1A'
LINK_GOOGLEAPI = 'https://maps.googleapis.com/maps/api/geocode/json?'



# resources cho database
MONGO_USER     = "the_phoenix"
MONGO_PASS     = "(^_^)W8ran!OzX4&TP7uKi$lKDLpHC@t_youwouldneverguessthisshit(^_^)"
MONGO_SERVER   = "35.198.246.144"

# DB lưu trữ các collection cần thiết
# MONGO_DB       = "real_estate_production"           # DB lưu trữ các collection cần thiết
MONGO_DB       = "AI"           # DB lưu trữ các collection cần thiết

# danh sách các collection

MONGO_COLL_ERRPOST  = "error_post"                  # collection lưu trữ các bài post sai
MONGO_COLL_GEO      = "geolocation"                 # collection lưu trữ các địa danh (surrounding_name, project)
MONGO_COLL_ERRGEO   = "error_geolocation"           # collection lưu trữ các địa danh không xác định được bằng Google Geolocation API



if ENV == "PROD":
    # MONGO_LINK = "mongodb://" + urllib.parse.quote(MONGO_USER) + ":" +  urllib.parse.quote(MONGO_PASS) + "@" + MONGO_SERVER + ":27019/default_db?authSource=real_estate_production"
    MONGO_LINK         = "mongodb://localhost:27017"
    MONGO_COLL_DATA    = "marker"
elif ENV == "DEV":
    MONGO_LINK         = "mongodb://localhost:27017"
    MONGO_COLL_DATA    = "marker"
else:
    logging.error("Bad ENV argument.")
    exit(1)
