import logging

import app.settings as settings

class PostInfo:
    def __init__(self):
        self._info = {
            "message":                          None,
            "attr_addr_number":                 None,
            "attr_addr_street":                 None,
            "attr_addr_district":               None,
            "attr_addr_ward":                   None,
            "attr_addr_city":                   None,
            "attr_position":                    None,
            "attr_surrounding":                 None,
            "attr_surrounding_name":            None,
            "attr_surrounding_characteristics": None,
            "attr_transaction_type":            None,
            "attr_realestate_type":             None,
            "attr_potential":                   None,
            "attr_area":                        None,
            "attr_price":                       None,
            "attr_price_min":                   None,
            "attr_price_max":                   None,
            "attr_price_m2" :                   None,
            "attr_interior_floor":              None,
            "attr_interior_room":               None,
            "attr_orientation":                 None,
            "attr_project":                     None,
            "attr_legal":                       None,
            'location_lng':                     None,
            'location_lat':                     None
        }

    def set_info_all(self, info):
        '''Gán các attribute của parameter `info` cho `self._info`
        '''
        for key, value in info.items():
            self._info[key] = value

    def get_info(self, attribute):
        return self._info[attribute]

    def set_info(self, attribute, value):
        self._info[attribute] = value

    def set_common_value(self):
        for key, value in self._info.items():
            if value == "":
                self._info[key] = None

    def get_info_all(self):
        return self._info
        
    def get_info_for_pushing(self):
        '''Get an info to push to Database
        :Return:
        - dict: nếu dữ liệu của post đủ informative để gửi lên DB
        - None: nếu dữ liệu không đủ informative (too few attributes have value) hoặc giá trị hash là None: có nghĩa là có post tương tự trên DB rồi
        '''


        
        tmp = self._info.copy()
        tmp['attr_area']      = 0 if tmp['attr_area'] is None or tmp['attr_area'] == "" else tmp['attr_area']
        tmp['attr_price_min'] = 0 if tmp['attr_area'] is None else tmp['attr_price_min']
        tmp['attr_price_max'] = 0 if tmp['attr_area'] is None else tmp['attr_price_max']
        tmp['attr_price_m2']  = 0 if tmp['attr_area'] is None else tmp['attr_price_m2']

        tmp['attr_surrounding']                 = tmp["attr_surrounding"]                 if tmp["attr_surrounding"] else None
        tmp['attr_surrounding_name']            = tmp["attr_surrounding_name"]            if tmp["attr_surrounding_name"] else None
        tmp['attr_surrounding_characteristics'] = tmp["attr_surrounding_characteristics"] if tmp["attr_surrounding_characteristics"] else None

        # check if this post is informative or not
        n_item_none = 0
        for _, value in self._info.items():
            if value is None or value == "" or value == 0:
                n_item_none += 1

        return tmp

    def get_n_attr_None(self):
        '''Trả về số lượng các attribute man giá trị None
        '''
        n = 0
        for _, value in self._info.items():
            if  value is None:
                n += 1
        return n