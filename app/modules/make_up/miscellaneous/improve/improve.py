from pymongo import MongoClient
from re import search
import logging
import numpy as np
import math
import re

from math import pi,sqrt,sin,cos,atan2
from app.modules.make_up.miscellaneous.improve.utils import find_anomalies
from app.modules.make_up.api.api_NLP_communicate import get_from_api
from app.modules.make_up.api.api_GGMap_communicate import get_from_ggmap
from app.modules.make_up.miscellaneous.normalize.utils import normalize_price
from app.modules.make_up.miscellaneous.regex_operation import clean_addr, clean_district, clean_full_address
from app.modules.make_up.utils import remove_accents, send_to_dbGeoErr, send_to_dbGeo, find_geolocation_by_name
from app.modules.make_up.miscellaneous.improve.LatLng import LatLng
import app.modules.make_up.miscellaneous.improve.convention as convention
from pymongo import MongoClient
client = MongoClient('localhost',27017)
db = client.AI
collect = db.marker
import app.settings as settings



def improve(post_info):
    '''Cải thiện các thuộc tính trong instance post_info thuộc class PostInfo
    '''

    fix_missing_attr_city(post_info)


    if post_info.get_info("attr_realestate_type") is None:
        fix_no_realestate_type(post_info)

    if post_info.get_info("attr_transaction_type") is None:
        fix_no_transaction_type(post_info)

    if post_info.get_info("attr_transaction_type") is not None:
        if post_info.get_info("attr_transaction_type").find(',') != -1:
            fix_many_transaction_type(post_info)

    if post_info.get_info("attr_price_min") is None:
        fix_no_price(post_info)


    # bước kiểm tra lỗi phụ
    if post_info.get_info("attr_transaction_type") == "mua":
        fix_specific_transaction_type(post_info)

    # bước này không hẳn là sửa lỗi, mà sẽ bổ sung lat, lng của bài post
    ############################################
    # HIỆN TẠI VÌ CHƯA HIỆN THỰC XONG NÊN CHỨC NĂNG NÀY TẠM KHÓA (AUG 29)
    # MỌI NGƯỜI KHI ĐÃ HIỆN THỰC XONG MỞ LẠI CHỨC NĂNG NÀY BẰNG CÁCH UNCOMMENT DÒNG BÊN DƯỚI
    # fill_lat_long(post_info)
    

# sửa lỗi không có `attr_realestate_type`
def fix_no_realestate_type(post_info):
    # solution 1: if post belongs to project, it will be set as "dự án"
    if post_info.get_info('attr_project') is not None and len(post_info.get_info('attr_project')) > 0:
        post_info.set_info("attr_realestate_type", "dự án")


    # solution 2: if post_message contains "dự án", it will be set as "dự án"
    if search(r"dự án", post_info.get_info('message')):
        post_info.set_info("attr_realestate_type", "dự án")
    
    # solution 3: if post_message contains "lô" or "nền", it will be set as "đất nền"
    if search(r"lô|nền", post_info.get_info('message')):
        post_info.set_info("attr_realestate_type", "đất nền")

    # solution 4: if post_message contains "căn", it will be set as "nhà"
    if search(r"căn", post_info.get_info('message')):
        post_info.set_info("attr_realestate_type", "nhà")

    # solution 5: if attr_interior_floor and attr_interior_room are available, it will be set as "nhà"
    if post_info.get_info('attr_interior_floor') is not None or post_info.get_info('attr_interior_room') is not None:
        post_info.set_info("attr_realestate_type", "nhà")


# sửa lỗi không có `attr_transaction_type`
def fix_no_transaction_type(post_info):
    # solution 1: if post belongs to project, it will be set as "bán"
    if post_info.get_info('attr_project') is not None and len(post_info.get_info('attr_project')) > 0:
        post_info.set_info("attr_transaction_type", "bán")

    # solution 2: if post' realestate_type is "đất", "nhà", "căn hộ", "mặt bằng", it will be set as "bán"
    if post_info.get_info('attr_realestate_type') in ["đất", "nhà", "căn hộ", "mặt bằng"]:
        post_info.set_info("attr_transaction_type", "bán")
    
    # solution 3: if post' realestate_type is "đất" or post_message contains word "lô", "nền" or "thổ cư", it will be set as "bán"
    if search(r"lô|nền|thổ cư", post_info.get_info('message')):
        post_info.set_info("attr_transaction_type", "bán")


# có một số bài đăng (đặc biệt là trên Facebook) người đăng 
def fix_many_transaction_type(post_info):
    tmp = post_info.get_info('attr_transaction_type')

    # solution 1: nếu có cả "thuê" và "bán":
    # * Nếu bài post có liệt kê cả giá thuê và giá bán thì chỉ hiển thị giá bán và transaction là bán
    #   ở đây cần phải xử lí lại nội dung bài post một chút xíu ở phần giá
    #   với những bài post có transaction_type là "thuê,bán":
    #   - trước tiên cần xét xem bài post có đề cập giá bán không, hay chỉ mỗi giá thuê
    #   - nếu có giá bán thì đặt transaction_type là "bán", đồng thời cập nhật lại attr_price_min với giá trị cao nhất
    # * Nếu bài post không đề cập giá thì mặc định để bán
    if search(r"thue.+ban|ban.+thue", tmp):
        price_list = get_from_api(post_info.get_info("message"))['attr_price']
        
        if len(price_list) > 0:
            price_sell = (0, "")

            # tìm giá cao nhất, xét xem nó có phải giá bán không (lớn hơn 500 triệu), nếu phải thì gán là bán, không thì để thuê
            for price in price_list:
                if normalize_price(price)[0] > price_sell[0]:
                    price_sell = (normalize_price(price)[0], price)

            if price_sell[0] > 500000000:
                # đây là giá bán, đặt lại giá trị của 4 attribute: attr_transaction_type, attr_price, attr_price_m2 và attr_price_min
                post_info.set_info("attr_transaction_type", "bán")            
                post_info.set_info("attr_price_min", price_sell[0])
                post_info.set_info("attr_price_m2", price_sell[0] / float(post_info.get_info('attr_area')))
                post_info.set_info("attr_price", price_sell[1])


    # solution 2: nếu chỉ có 2 loại xuất hiện là "mua" và "bán" thì đặt là "bán"
    if search(r"thuê|nhượng|khác|kí gửi", tmp) is None:
        post_info.set_info("attr_transaction_type", "bán")

        
    # solution 3: nếu trong số các transaction_type được liệt kê, có "bán", "nhượng" hoặc "thuê", và 3 loại đó xuất hiện đầu tiên trong bài post, 
    # thì transaction_type sẽ đặt là 3 loại đó
    if search(r"nhượng", tmp):
        if search(r"nhượng", tmp).span()[0] == 0:
            post_info.set_info("attr_transaction_type", "nhượng")

    if search(r"thuê", tmp):
        if search(r"thuê", tmp).span()[0] == 0:
            post_info.set_info("attr_transaction_type", "thuê")

    
    # solution 4:

    if search(r"bán", tmp):
        post_info.set_info("attr_transaction_type", "bán")

    if search(r"mua", tmp):
        post_info.set_info("attr_transaction_type", "mua")


def fix_incorrect_price(post_info):
    # solution 1: kiểm tra xem giá có là outlier không
    client = MongoClient(settings.MONGO_LINK)
    collection = client[settings.MONGO_DB][settings.MONGO_DATA]
    result = collection.aggregate([
        { "$match" : {"page" : "https://landber.com"} },
        { "$group" : {
            "_id" : None,
            "price_min": {"$push" : "$attr_price_min"}
        }}
    ])

    for tmp in result:
        price_min_list = list(filter(None, tmp['price_min']))

    
    # Solution 2:
    # với những bài có nhiều giá, giá đưa ra đầu tiên chưa chắc là giá đúng, mình cần phân tích lại dữ liệu để tìm giá đúng
    # trường hợp này tập trung giải quyết giá nhà/đất quá thấp (cụ thể là dưới 500 trieu)
    if post_info.get_info("attr_transaction_type") == "bán" and post_info.get_info("attr_price_min") < 500000000:
        attributes = get_from_api(post_info.get_info("message"))

        price_list = attributes['attr_price']
        for price in price_list:
            price_min, _, price_min_m2, _, _ = normalize_price(price)
            if price_min is not None and price_min > 500000000:
                post_info.set_info('attr_price', price)
                post_info.set_info('attr_price_min', price_min)
                try:
                    post_info.set_info('attr_price_m2', price_min/float(post_info.get_info("attr_area")))
                except Exception:
                    pass
                break
            elif price_min_m2 is not None and post_info.get_info("attr_area") is not None and price_min_m2 * float(post_info.get_info("attr_area")) > 500000000:
                price_min = price_min_m2 * float(post_info.get_info("attr_area"))
                post_info.set_info('attr_price', str(price_min))
                post_info.set_info('attr_price_min', price_min)
                post_info.set_info('attr_price_m2', price_min_m2)
                break

# hàm này không hẳn là sửa lỗi, mà sẽ thay đổi `attr_transaction_type` dựa vào một vài chi tiết trong bài post
def fix_specific_transaction_type(post_info):
    # kiểm tra xem nội dung của post có chứa một trong các chữ dưới đây không (đây là các chữ tiêu biểu cho bài rao về mua,
    # nếu không có rất có khả năng post là về "bán" chứ không phải "mua")
    if search(r"cần mua|muốn mua|khách|nét|inbox|ib", post_info.get_info("message")) is None:
        post_info.set_info("attr_transaction_type", "bán")

    # Solution 2: Nếu số lượng các attribute mang giá trị None ít thì nhiều khả năng đây là post bán
    if post_info.get_n_attr_None() < 8:
        post_info.set_info("attr_transaction_type", "bán")

# sửa lỗi bất động sản không có giá
def fix_no_price(post_info):
    # Solution 1: nếu bài post có sẵn diện tính và giá mỗi mét vuông thì tính giá
    if post_info.get_info("attr_price_m2") and post_info.get_info("attr_area"):
        price = post_info.get_info("attr_price_m2") * float(post_info.get_info("attr_area"))
        post_info.set_info("attr_price", str(price))
        post_info.set_info("attr_price_min", price)


# sửa attr_addr_city
# một số địa danh mình biết chắc nó phải thuộc thành phố/tỉnh 
def fix_missing_attr_city(post_info):
    if post_info.get_info('attr_addr_district') is None:
        return
    
    for province, geolocation in convention.GEOLOCATION_LIST.items():
        for attribute, value in geolocation.items():
            tmp = remove_accents(post_info.get_info(attribute))
            for pattern in value:
                if search(pattern, tmp):
                    post_info.set_info('attr_addr_city', province)
                    return


#########################################################################
######################### bổ sung lat, lng ##############################

def check_valid_lat_lng(lat_lng):
    '''Kiểm tra xem lat, lng có đúng không
    Lat, lng đúng là:
    - lat, lng đều là số thực dương
    - lat nằm trong khoảng (9.17682, 22.82333) và longitude trong khoảng (103.02301, 109.32094)
    :Args:
    - lat, lng - lat, lng của điểm cần kiểm tra
    :Rets:
    - True - nếu lat, lng là hợp lệ
    - False - trong trường hợp còn lại
    '''
    try:
        # hàm `try` sẽ loại bỏ trường hơp lat hoặc lng là None hoặc str, hàm `try` chỉ thoát `except` trong 2 trường hợp:
        # một là: một trong hai là str, cái còn lại là số nguyên
        # hai là: cả hai là số thực
        lat = lat_lng['lat']
        lng = lat_lng['lng']
        if type(lat*lng) is not str:
            if 9.17682 <= lat <= 22.82333 and 103.02301 <= lng <= 109.32094:
                return True
            else:
                return False
        else:
            return False
    except TypeError:
        return False


def get_full_address(post_info):
    '''Xác định địa chỉ của bất động sản dựa vào số nhà, tên đường, phường/xã, quận/huyện và thành phố
    :Args:
    - post_info - instance của class PostInfo
    :Rets:
    str - địa chỉ của bđs
    '''

    # this variable contains the address which is made up from 4 attributes mentioned above
    full_addr = "" 

    for tag in ['attr_addr_number', 'attr_addr_street', 'attr_addr_ward', 'attr_addr_district', 'attr_addr_city']:
        clean_tag = post_info.get_info(tag) if post_info.get_info(tag) is not None else ''
        if clean_tag == '':
            continue
        
        if tag == 'attr_addr_number':
            tmp = clean_addr(clean_tag)
            if len(tmp) != 0:
                full_addr = full_addr + tmp + ' '
        elif tag == 'attr_addr_ward':
            tmp = clean_addr(clean_tag)
            if len(tmp) != 0:
                full_addr = full_addr + ' phường ' + tmp + ' '
        elif tag == 'attr_addr_district':
            tmp = clean_district(clean_tag)
            if len(tmp) != 0:
                full_addr = full_addr + ' ' + tmp + ' '
        elif tag == "attr_addr_street":
            full_addr = full_addr + " đường " + clean_tag + ' '
        else:
            full_addr = full_addr + ' ' + clean_tag + ' '

    return full_addr.replace('  ', ', ')

def fill_lat_long(post_info):
    '''Xác định lat, lon của bất động sản

    Chi tiết thuật toán xem trong document
    '''
    # Nhắc lại một lần là lat, lng của bất động sản sẽ được tính dựa vào lat, lng của 3 nguồn
    # - lat, lng lấy từ dự án nếu bất động sản đó thuộc một dự án nào đó
    # - lat, lng của địa chỉ, ví dụ: địa chỉ 181 đường 3 tháng 2, lấy địa chỉ này cho đi qua Google API để tìm lat, lng
    # - lat, lng của các surrounding_name, ví dụ: nhà hát Hòa Bình, Kì Hòa... Mỗi surrounding name sẽ có một lat, lng riêng

    ######################################################
    ### Kiểm tra bài post có thuộc dự án nào không #######
    ######################################################
    if post_info.get_info('attr_project'):
        # tiến hành kiểm tra trên DB (collection geolocation), xem nếu có dự án đó tồn tại thì lấy lat, lng của dự án đó về
        lat_lng = find_geolocation_by_name(post_info.get_info('attr_project'), post_info.get_info('attr_addr_district'), post_info.get_info('attr_addr_city'))

        if lat_lng:
            post_info.set_info('location_lat', lat_lng[0])
            post_info.set_info('location_lng', lat_lng[1])

            return

        else:
            # không tồn tại tên dự án trên DB, nên ta thử kết hợp các yếu tố quận/huyện, thành phố để tìm lat, lng của dự án bằng GG Geo API
            str_find_GGAPI = post_info.get_info('attr_project') + ''
            if post_info.get_info('attr_addr_district'):
                str_find_GGAPI = str_find_GGAPI + ", " + post_info.get_info('attr_addr_district')
            if post_info.get_info('attr_addr_city'):
                str_find_GGAPI = str_find_GGAPI + ", " + post_info.get_info('attr_addr_city')

            # tìm với GG API
            result = get_from_ggmap(str_find_GGAPI)
            if check_valid_lat_lng(result):
                # đẩy địa danh mới tìm được lên DB Geo
                send_to_dbGeo(post_info.get_info('attr_project'), post_info.get_info('attr_addr_street'),
                              post_info.get_info('attr_addr_district'), post_info.get_info('attr_addr_ward'), post_info.get_info('attr_addr_city'), result['lat'], result['lng'])

                post_info.set_info('location_lat', result['lat'])
                post_info.set_info('location_lng', result['lng'])
                return

            else:
                # đã tìm bằng GG API mà vẫn không tìm thấy hoặc kết quả trả về không hợp lệ
                # đẩy tên dự án vào DB Geolocation, sau đó tiếp tục
                send_to_dbGeoErr(post_info.get_info('attr_project'), post_info.get_info('attr_addr_street'),
                                 post_info.get_info('attr_addr_district'), post_info.get_info('attr_addr_ward'), post_info.get_info('attr_addr_city'))
    
    

    ######################################################
    ##### Kiểm tra xem post đã có sẵn lat, lng chưa ######
    ######################################################

    # instance này sẽ được dùng để tính lat, lng.
    lat_lng = LatLng()

    if post_info.get_info('location_lat') is not None:
        # đẩy vào list
        # push_lat_lng_to_stack(post_info.get_info('location_lat'), post_info.get_info('location_lng'))
        lat_lng.add_address_point(post_info.get_info('location_lat'), post_info.get_info('location_lng'))
    else:
        # kiểm tra xem có các yếu tố đường, phường/xã, quận/huyện, thành phố không
        str_find_GGAPI = get_full_address(post_info)
        if len(str_find_GGAPI) > 0:
            result = get_from_ggmap(str_find_GGAPI)
            if check_valid_lat_lng(result):
                # đẩy lat, lng vào list
                # push_lat_lng_to_stack(result['lat'], result['lng'])
                lat_lng.add_address_point(result['lat'], result['lng'])
            
                # nếu có số nhà cụ thể, thì lat, lng này đúng rồi
                if post_info.get_info('attr_addr_number') is not None:
                    # tính toán lat, lng
                    post_info.set_info('location_lat', result['lat'])
                    post_info.set_info('location_lng', result['lng'])
                    return



    ####################################################
    #### kiểm tra xem có surrounding_name không ########
    ####################################################
    if post_info.get_info('attr_surrounding_name'):
        for surrounding_name in post_info.get_info('attr_surrounding_name').split(', '):
            result = find_geolocation_by_name(surrounding_name, post_info.get_info('attr_addr_district'), post_info.get_info('attr_addr_city'))
            if result:
                # push_lat_lng_to_stack(result[0], result[1])
                lat_lng.add_surrounding_point(result[0], result[1])
            else:
                str_find_GGAPI = surrounding_name + '' 
                if post_info.get_info('attr_addr_ward'):
                    str_find_GGAPI = str_find_GGAPI + ", " + post_info.get_info('attr_addr_ward')
                if post_info.get_info('attr_addr_district'):
                    str_find_GGAPI = str_find_GGAPI + ", " + post_info.get_info('attr_addr_district')
                if post_info.get_info('attr_addr_city'):
                    str_find_GGAPI = str_find_GGAPI + ", " + post_info.get_info('attr_addr_city')

                    result = get_from_ggmap(str_find_GGAPI)
                if check_valid_lat_lng(result):
                    send_to_dbGeo(surrounding_name, post_info.get_info('attr_addr_street'),
                              post_info.get_info('attr_addr_district'), post_info.get_info('attr_addr_ward'), post_info.get_info('attr_addr_city'), result['lat'], result['lng'])

                    # đẩy lat, lng vào list
                    # push_lat_lng_to_stack(result['lat'], result['lng'])
                    lat_lng.add_surrounding_point(result['lat'], result['lng'])
                else:    
                    send_to_dbGeoErr(surrounding_name, post_info.get_info('attr_addr_street'),
                                 post_info.get_info('attr_addr_district'), post_info.get_info('attr_addr_ward'), post_info.get_info('attr_addr_city'))
        
        tmp = lat_lng.calculate()
        post_info.set_info('location_lat', tmp[0])
        post_info.set_info('location_lng', tmp[1])
        return
    else:
        tmp = lat_lng.calculate()
        if tmp:
            post_info.set_info('location_lat', tmp[0])
            post_info.set_info('location_lng', tmp[1])
            return
        else:
            # bài post không tồn tại bất kì yếu tố nào có thể dùng để xác định lat, lng ==> Đây là một bài post không hợp lệ
            # đưa bài post này lên DB chứa các bài post lỗi

            # Aug 26: dòng sau đang bị deactivated
            # send_to_dbPostErr(post_info)
            pass

def distance(pos1, pos2):
    lat1 = float(pos1['location_lat'])
    long1 = float(pos1['location_lng'])
    if (type(pos2) is tuple):
        lat2 = float(pos2[0])
        long2 = float(pos2[1])
    else: 
        lat2 = float(pos2['lat'])
        long2 = float(pos2['lng'])
    degree_to_rad = float(pi / 180.0)

    d_lat = (lat2 - lat1) * degree_to_rad
    d_long = (long2 - long1) * degree_to_rad

    a = pow(sin(d_lat / 2), 2) + cos(lat1 * degree_to_rad) * cos(lat2 * degree_to_rad) * pow(sin(d_long / 2), 2)
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    km = 6367 * c
    mi = 3956 * c

    return {"km":km, "miles":mi}

def checklocation(pos1):    
    lat_lng = LatLng()
    data = {}
    data.update(pos1)
    if(re.search(r'facebook',data['page'])):

        if (data["attr_surrounding_name"] is None ):
            data["attr_surrounding_name"] = ""

        if (data["attr_surrounding"] is None ):
            data["attr_surrounding"] = ""

        if (data["attr_addr_number"] is None ):
            data["attr_addr_number"] = ""    

        if (data["attr_addr_district"] is None ):
            data["attr_addr_district"] = ""

        if (data["attr_addr_street"] is None ):
            data["attr_addr_street"] = ""
            
        if (data["attr_addr_city"] is None ):
            data["attr_addr_city"] = ""    
        
        if (data["attr_addr_ward"] is None ):
            data["attr_addr_ward"] = ""   


        if (data["location_lng"] is None or data["location_lng"] is None):

            addr_street = data["attr_addr_street"] + ", " + data["attr_addr_ward"] + ", " + data["attr_addr_district"]
            street = get_from_ggmap(addr_street)
            surr = data["attr_surrounding"].split(',')
            surr_name = data["attr_surrounding_name"].split(',')
            for i in range(min(len(surr),len(surr_name))):
                res= surr[i] + surr_name[i]
                if get_from_ggmap(res)  != None:
                    lat = get_from_ggmap(res)['lat']
                    lng = get_from_ggmap(res)['lng']
                    lat_lng.add_surrounding_point(lat,lng,0)
                    lat_lng.add_address_point(0,0)
                    addr_point = (lat_lng.calculate())

            if (street  is None):
                print("New location point: ", addr_point)
                data['location_lat'] = addr_point[0]
                data['location_lng'] = addr_point[1]
            else:
                if ( distance(data, addr_point)['km'] > distance(data,street)['km']):
                    print("New location street: ", street)
                    data['location_lat'] = street['lat']
                    data['location_lng'] = street['lng']

                else:
                    print("New location point: ", addr_point)
                    data['location_lat'] = addr_point['0']
                    data['location_lng'] = addr_point['1']
                            
                if (addr_point[0] is None):
                    print('New location: ',street)
                    data['location_lat'] = street['lat']
                    data['location_lng'] = street['lng']
                else:
                    if ( distance(data, addr_point)['km'] > distance(data,street)['km']):
                        print("New location street: ", street)
                        data['location_lat'] = street['lat']
                        data['location_lng'] = street['lng']

                    else:
                        print("New location point: ", addr_point)
                        data['location_lat'] = addr_point['0']
                        data['location_lng'] = addr_point['1']

        else:
            if (data["location_lat"] is None and data["location_lng"] is None ):

                addr_street = data["attr_addr_street"] + ", " + data["attr_addr_ward"] + ", " + data["attr_addr_district"]
                street = get_from_ggmap(addr_street)

            surr = data["attr_surrounding"].split(',')
            surr_name = data["attr_surrounding_name"].split(',')
            if (data["attr_addr_number"]):
                addr_number = data["attr_addr_number"] + data["attr_addr_street"] + data["attr_addr_ward"] + data["attr_addr_district"]
                print(get_from_ggmap(addr_number))
            else:
                if(data["attr_surrounding"] != "None" and data["attr_surrounding_name"] != "None"):
                    for i in range(min(len(surr),len(surr_name))):
                        res= surr[i] + surr_name[i]
                        if get_from_ggmap(res)  != None:
                            lat = get_from_ggmap(res)['lat']
                            lng = get_from_ggmap(res)['lng']
                            lat_lng.add_surrounding_point(lat,lng,0)
                            lat_lng.add_address_point(0,0)
                    addr_point = (lat_lng.calculate())
                    addr_street = data["attr_addr_street"] + ", " + data["attr_addr_ward"] + ", " + data["attr_addr_district"]
                    street = get_from_ggmap(addr_street)

                    if (street is None):
                        print("New location point: ", addr_point)
                        data['location_lat'] = addr_point[0]
                        data['location_lng'] = addr_point[1]
                    elif addr_point[0] is not None:
                        if ( distance(data, addr_point)['km'] > distance(data,street)['km']):
                            print("New location street1: ", street)
                            data['location_lat'] = street['lat']
                            data['location_lng'] = street['lng']

                        else:
                            print("New location point: ", addr_point)
                            data['location_lat'] = addr_point['0']
                            data['location_lng'] = addr_point['1']
                                
                        if (addr_point[0] is None):
                            print('New location: ',street)
                            data['location_lat'] = street['lat']
                            data['location_lng'] = street['lng']
                    # elif street['lat'] is not None:
                    #     # print( addr_point)
                    #     if ( distance(data, addr_point)['km'] > distance(data,street)['km']):
                    #         print("New location street: ", street)
                    #         data['location_lat'] = street['lat']
                    #         data['location_lng'] = street['lng']

                    #     else:
                    #         print("New location point: ", addr_point)
                    #         data['location_lat'] = addr_point['0']
                    #         data['location_lng'] = addr_point['1']

                elif (data["attr_surrounding"] == "None"):
                    for i in range(len(surr_name)):
                        res=  surr_name[i]
                        if get_from_ggmap(res)  != None:
                            lat = get_from_ggmap(res)['lat']
                            lng = get_from_ggmap(res)['lng']
                            lat_lng.add_surrounding_point(lat,lng,0)
                            lat_lng.add_address_point(0,0)
                    addr_point = (lat_lng.calculate())
                    addr_street = data["attr_addr_street"] + ", " + data["attr_addr_ward"] + ", " + data["attr_addr_district"]
                    street = get_from_ggmap(addr_street)

                    if (street['lat'] is None):
                        print("New location point: ", addr_point)
                        data['location_lat'] = addr_point[0]
                        data['location_lng'] = addr_point[1]

                    elif (addr_point[0] is not None):
                        if ( distance(data, addr_point)['km'] > distance(data,street)['km']):
                            print("New location street: ", street)
                            data['location_lat'] = street['lat']
                            data['location_lng'] = street['lng']

                        else:
                            print("New location point: ", addr_point)
                            data['location_lat'] = addr_point['0']
                            data['location_lng'] = addr_point['1']

                        if (addr_point[0] is None):
                            print('New location street: ',street)
                            data['location_lat'] = street['lat']
                            data['location_lng'] = street['lng']
                    # elif (street['lat'] is not None):
                    #     # print( addr_point)
                    #     if ( distance(data, addr_point)['km'] > distance(data,street)['km']):
                    #         print("New location street: ", street)
                    #         data['location_lat'] = street['lat']
                    #         data['location_lng'] = street['lng']

                    #     else:
                    #         print("New location point: ", addr_point)
                    #         data['location_lat'] = addr_point['0']
                    #         data['location_lng'] = addr_point['1']

                elif (data["attr_surrounding_name"] == "None"):
                    for i in range(len(surr)):
                        res=  surr_name[i]
                        if get_from_ggmap(res)  != None:
                            lat = get_from_ggmap(res)['lat']
                            lng = get_from_ggmap(res)['lng']
                            lat_lng.add_surrounding_point(lat,lng,0)
                            lat_lng.add_address_point(0,0)
                    addr_point = (lat_lng.calculate())
                    addr_street = data["attr_addr_street"] + ", " + data["attr_addr_ward"] + ", " + data["attr_addr_district"]
                    street = get_from_ggmap(addr_street)

                    if (street['lat'] is None):
                        print("New location point: ",addr_point)
                        data['location_lat'] = addr_point[0]
                        data['location_lng'] = addr_point[1]

                    elif (addr_point['lat'] is not None):
                        if ( distance(data, addr_point)['km'] > distance(data,street)['km']):
                            print("New location street: ", street)
                            data['location_lat'] = street['lat']
                            data['location_lng'] = street['lng']

                        else:
                            print("New location point: ", addr_point)
                            data['location_lat'] = addr_point['0']
                            data['location_lng'] = addr_point['1']
                            
                        if (addr_point[0] is None):
                            
                            print('New location: ',street)
                            data['location_lat'] = street['lat']
                            data['location_lng'] = street['lng']
                    # elif (street['lat'] is not None):

                    #     if ( distance(data, addr_point)['km'] > distance(data,street)['km']):
                    #         print("New location street: ", street)
                    #         data['location_lat'] = street['lat']
                    #         data['location_lng'] = street['lng']

                    #     else:
                    #         print("New location point: ", addr_point)
                    #         data['location_lat'] = addr_point['0']
                    #         data['location_lng'] = addr_point['1']

    mu, sigma = 0, 0.1
    eps = (np.random.normal(mu, sigma, 1))
    data['location_lng'] =  data['location_lng'] + eps[0] * float(10^-4)
    data["location lat"] = data['location_lat'] + eps[0] * float(10^-4)
    print("location lat: ", data['location_lat'])
    print("location lng: ", data['location_lng'])
    # print(eps)
    # if (type(addr_point) is list):
    #     new_lat = collect.find_one_and_update({"location_lat":street[0]})
    #     new_lng =  collect.find_one_and_update({"location_lat":street[1]})