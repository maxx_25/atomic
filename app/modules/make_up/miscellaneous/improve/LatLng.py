import numpy as np
import math
import threading

R_EARTH      = 6380 * 1000              # bán kính trái đất, tính theo đơn vị meter
RUNNING_TIME = 10000                    # số lần chạy để tính lat, lng của bất động sản
MOVEMENT     = 0.0001                   # đây là khoảng cách sẽ di chuyển điểm lat, lng của bất động sản
THRESHOLD    = 3000                     # ngưỡng này là khoảng cách (đơn vị meter) dùng để xác định surrounding và addr point là xa hay gần điểm surrounding trung tâm,
                                        # đồng thời cũng được dùng để tìm điểm đại diện của các surrounding hay addr point ở xa
SIGMA        = 0.1
class LatLng:
    def __init__(self):
        self.surrounding_point_list = None   # list chứa tuple gồm (lat, lng, distance) của từng surrounding point
        self.addr_point             = None   # là tuple chứa (lat, lng) của điểm địa chỉ

        self.realestate_lat         = None   # latitude của bất động sản
        self.realestate_lng         = None   # longitude của bất động sản
    
    def metric(self, lat, lng):
        # biến này sẽ chứa tổng của khoảng cách
        tmp = 0
        for surr_point in self.surrounding_point_list:
            tmp += abs(self.distance(lat, lng, surr_point[0], surr_point[1]) - surr_point[2])

        return tmp

    def distance(self, x_A, y_A, x_B, y_B):
        '''Tính khoảng cách giữa 2 điểm A và B
        '''
        return math.sqrt((x_A - x_B)**2 + (y_A - y_B)**2)

    def convert(self, distance_meter):
        '''Đổi khoảng cách từ meter sang đơn vị dùng trong hệ tọa độ lat, lng
        '''

        return distance_meter * 360 / (2 * math.pi * R_EARTH)

    def find_representing_point(self, x_A, y_A, x_B, y_B, distance):
        '''Tìm tọa độ của điểm C trên đoạn thẳng AB sao cho độ dài đoạn thẳng AC = distance

        :Args:
        - x_A, y_A, x_B, y_B - tọa độ của 2 điểm A, B
        - distance - độ dài đoạn thẳng AC

        :Rets:
        - None, None nếu distance >= AB (độ dài của AC >= độ dài của AB)
        - float, float trong trường hợp thành công
        '''
        if distance >= self.distance(x_B, y_B, x_A, y_A):
            return None, None
        # gọi x_C, y_C là tọa độ của điểm cần tìm C
        # coef_x, coef_y là hệ số để tính toán tọa độ của C, chi tiết xem trong document
        coef_x = 1 if x_B > x_A else -1
        coef_y = 1 if y_B > y_B else -1
        x_C    = coef_x * abs(x_B - x_A) * distance / self.distance(x_B, y_B, x_A, y_A) + x_A
        y_C    = coef_y * abs(y_B - y_A) * distance / self.distance(x_B, y_B, x_A, y_A) + y_A
    
        return x_C, y_C

    
    
    def add_surrounding_point(self, point_lat, point_lng, point_distance):
        '''Thêm điểm surrounding vào list

        :Args:
        - point_lat, point_lng - lat, lng của surrouding point
        - point_distance - khoảng cách (đơn vị meter)
        :Rets:
        - True - arguments không None
        - False - trong các trường hợp còn lại
        '''
        if point_lat is None or point_lng is None or point_distance is None:
            return False
        

        if self.surrounding_point_list is None:
            self.surrounding_point_list = list()
        
        # vì 'point_distance' ở đơn vị meter nên cần đổi qua đơn vị độ để phù hợp với lat, lng
        point_distance = self.convert(point_distance)

        # thêm zô list
        self.surrounding_point_list.append([point_lat, point_lng, point_distance])

    def add_address_point(self, addr_point_lat, addr_point_lng):
        '''Gán điểm address

        :Args:
        - point_lat, point_lng - lat, lng của address point
        :Rets:
        - True - arguments không None
        - False - trong các trường hợp còn lại
        '''
        if addr_point_lat is None or addr_point_lng is None:
            return False

        self.addr_point = [addr_point_lat, addr_point_lng]
        


    def calculate(self):
        '''Tính toán toạ độ lat, lng hợp lí nhất của bất động sản

        :Args: void

        :Rets:
        - (lat, lng) - toạ độ của bất động sản
        - (None, None) - trong trường hợp không có surrounding và address
        '''

        if self.surrounding_point_list is None:
            if self.addr_point is None:
                return None, None
            else:
                return self.addr_point[0], self.addr_point[1]


        if self.addr_point is not None:
            # Tìm điểm surrounding trung tâm và tìm điểm đại diện của các surrrounding xa

            # đầu tiên, tìm điểm surrounding trung tâm
            # điểm surrounding trung tâm là điểm surrounding gần với vị trí của address nhất
            surr_main_lat, surr_main_lng = 0, 0
            distance = 100000                                    # biến này chứa khoảng cách từ điểm surrounding trung tâm tới điểm address 
            for surr_point in self.surrounding_point_list:
                tmp = self.distance(surr_point[0], surr_point[1], self.addr_point[0], self.addr_point[1])
                if distance > tmp:
                    distance = tmp
                    surr_main_lat, surr_main_lng = surr_point[0], surr_point[1]

            # tiếp theo, kiểm tra nếu khoảng cách từ điểm addr tới điểm surrounding trung tâm quá xa thì tìm điểm đại diện của điểm addr
            if self.distance(self.addr_point[0], self.addr_point[1], surr_main_lat, surr_main_lng) >= self.convert(THRESHOLD):
                self.addr_point[0], self.addr_point[1] = self.find_representing_point(surr_main_lat, surr_main_lng, self.addr_point[0], self.addr_point[0], self.convert(THRESHOLD))

            # kiểm tra từng điểm surrounding, nếu có điểm nào ở xa so với điểm surrounding trung tâm thì tìm điểm đại diện của điểm đó
            for surr_point in self.surrounding_point_list:
                if self.distance(surr_point[0], surr_point[1], surr_main_lat, surr_main_lng) >= self.convert(THRESHOLD):
                    surr_point[0], surr_point[1] = self.find_representing_point(surr_main_lat, surr_main_lng, surr_point[0], surr_point[1], self.convert(THRESHOLD))


        #####################################################
        ## Tìm điểm surrounding point trung tâm             #
        #####################################################
        lat_list, lng_list = list(), list()
        for lat_lng in self.surrounding_point_list:
            lat_list.append(lat_lng[0])
            lng_list.append(lat_lng[1])
        
        self.realestate_lat = np.mean(lat_list)
        self.realestate_lng = np.mean(lng_list)


        #####################################################
        ## Tìm điểm đại diện cho address point              #
        #####################################################
        # điểm đại diện cho address point sau khi được tìm ra sẽ thêm vào list của các điểm surrounding
        self.addr_point[0] = SIGMA * (self.addr_point[0] - self.realestate_lat) + self.realestate_lat
        self.addr_point[1] = SIGMA * (self.addr_point[1] - self.realestate_lng) + self.realestate_lng

        self.surrounding_point_list.append((self.addr_point[0], self.addr_point[1], 0))


        #####################################################
        ## Tính toán điểm cần tìm                           #
        #####################################################
        # Đến đây
        for i in range(RUNNING_TIME):
            if i % 2 == 0:
                result1 = self.metric(self.realestate_lat + MOVEMENT, self.realestate_lng)
                result2 = self.metric(self.realestate_lat - MOVEMENT, self.realestate_lng)

                if result1 < result2:
                    self.realestate_lat += MOVEMENT
                else:
                    self.realestate_lat -= MOVEMENT
            else:
                result1 = self.metric(self.realestate_lat, self.realestate_lng + MOVEMENT)
                result2 = self.metric(self.realestate_lat, self.realestate_lng - MOVEMENT)

                if result1 < result2:
                    self.realestate_lng += MOVEMENT
                else:
                    self.realestate_lng -= MOVEMENT

        return self.realestate_lat, self.realestate_lng