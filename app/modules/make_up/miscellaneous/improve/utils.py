import logging

# Function to Detection Outlier on one-dimentional datasets.
def find_anomalies(price_data_std, price_data_mean, check_value):
    anomaly_cut_off = price_data_std * 3
    
    lower_limit = price_data_mean - anomaly_cut_off 
    upper_limit = price_data_mean + anomaly_cut_off
    print("upper_limit: " , upper_limit)
    print("lower_limit: " , lower_limit)


    if check_value > upper_limit or check_value < lower_limit:
        return True
    else:
        return False

