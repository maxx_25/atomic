GEOLOCATION_LIST = {
    "hồ chí minh" : {
        "attr_addr_district" : [
            r"qu.n \d{1,2}",
            r"tan binh",
            r"go vap",
            r"binh tan",
            r"binh thanh",
            r"thu duc",
            r"phu nhuan",
            r"tan phu",
            r"cu chi",
            r"hoo?c mon",
            r"con gio",
            r"nha be",
            r"binh chanh"
        ]
    },
    "đồng nai" : {
        "attr_addr_district" : [
            r"thong nhat",
            r"long thanh",
            r"vinh cuu",
            r"dinh quan",
            r"xuan loc",
            r"cam my",
            r"trang bom",
            r"tan phu",
            r"nhon trach"
        ]
    },
    "hà nội" : {
        "attr_addr_district" : [
            r"thanh xuan",
            r"hoang mai",
            r"cau giay",
            r"nam tu liem",
            r"bac tu liem",
        ]
    }

}