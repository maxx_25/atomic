from re import findall, search, sub
import logging

from app.modules.make_up.api.api_NLP_communicate import get_from_api
from app.modules.make_up.utils import remove_accents

# mặc dù cũng thuộc nhóm chức năng chuẩn hóa, nhưng hàm 'normalize_price' lại được gọi ở đây vì như thế sẽ tiện hơn
from app.modules.make_up.miscellaneous.normalize.utils import normalize_price


import app.settings as settings


filling_attrs = [
    "attr_addr_number",
    "attr_addr_street",
    "attr_addr_district",
    "attr_addr_ward",
    "attr_addr_city",
    "attr_position",
    "attr_surrounding",
    "attr_surrounding_name",
    "attr_surrounding_characteristics",
    "attr_transaction_type",
    "attr_realestate_type",
    "attr_potential",
    "attr_area",
    "attr_price",
    "attr_price_min",
    "attr_price_max",
    "attr_price_m2",
    "attr_interior_floor",
    "attr_interior_room",
    "attr_orientation",
    "attr_project",
    "attr_legal"
]


def get_attribute(post_info):
    '''Dùng NLP API và Geo Google API để trích xuất các thuộc tính từ nội dung bài post
    :Args:
    - post_info: instance thuôc class PostInfo
    :Rets:
    '''

    # phân tích các thuộc tính của bài post bằng cách dùng NLP API để phân giải thuộc tính và Geo API để phân giải kinh độ, vĩ độ
    attributes = extract_attributes(post_info)

    # print("msg : ", post_info.get_info("message"))



    price_min, price_max, price_m2, area_tmp, price_str = get_price(post_info, attributes)
    area           = get_area(post_info, attributes)
    interior_floor = get_interior_floor(post_info, attributes)

    # cập nhật area hoặc price_m2
    if area == 0 and area_tmp != 0 and area_tmp:
        area = float(area_tmp)
    # nhân diện tích với số tầng để tìm ra diện tích thật của bất động sản
    area = area * interior_floor

    if price_m2 == 0 and price_min != 0 and area != 0:
        price_m2 = price_min / area


    # sau khi đã phân tích nội dung bài post bằng NLP API và các thuộc tính đã được lưu trong 'attributes',
    # gán chúng vào instace class Post
    for attribute in filling_attrs:
        if attribute == "attr_price_min":
            post_info.set_info(attribute, price_min)
        elif attribute == "attr_price_max":
            post_info.set_info(attribute, price_max)
        elif attribute == "attr_price_m2":
            post_info.set_info(attribute, price_m2)
        elif attribute == "attr_area":
            post_info.set_info(attribute, area)
        elif attribute == "attr_price":
            post_info.set_info(attribute, price_str)
        elif attribute == "attr_interior_floor":
            post_info.set_info(attribute, interior_floor)

        elif post_info.get_info(attribute) is None:
            if attributes[attribute] == "":
                post_info.set_info(attribute, None)
            else:
                post_info.set_info(attribute, attributes[attribute])


def extract_attributes(post_info):
    '''Dùng NLP API để phân tích các thuộc tính
    '''
    tmp = get_from_api(post_info.get_info('message'))

    if post_info.get_info('attr_transaction_type'):
        tmp['attr_transaction_type'] = post_info.get_info('attr_transaction_type')

    if post_info.get_info('attr_realestate_type'):
        tmp['attr_realestate_type'] = post_info.get_info('attr_realestate_type')

    if post_info.get_info('attr_area'):
        tmp['attr_area'] = post_info.get_info('attr_area')

    if post_info.get_info('attr_legal'):
        tmp['attr_legal'] = post_info.get_info('attr_legal')

    if post_info.get_info('attr_orientation'):
        tmp['attr_orientation'] = post_info.get_info('attr_orientation')

    return tmp    


def get_price(post_info, attributes):
    if post_info.get_info('attr_price') is not None:
        return normalize_price(post_info.get_info('attr_price'))
    else:
        price_min = 0
        price_max = 0
        price_m2  = 0
        area_tmp  = 0
        price_str = ""

        for tmp in attributes['attr_price']:
            price = normalize_price(tmp)
            if price_min == 0 and price[0]:
                price_str = tmp
                price_min = price[0]
            if price_max == 0 and price[1]:
                if price_min > 0 and price_max > price_min:
                    price_max = price[1]
            if price_m2 == 0 and price[2]:
                price_m2 = price[2]
            if area_tmp == 0 and price[4]:
                area_tmp = price[4]

        # if reach here that means none of attribute related to price extracted by NLP API is valuable
        return price_min, price_max, price_m2, area_tmp, price_str




# regex for area
re_area = r"(\d+\.\d+|\d+)"

def extract_area(str_area):
    str_area = str_area.replace(" ", "").replace(",", ".").replace("m2", "").replace("m", "").replace("ha", "0000")
    #print(str_area)
    if search(pattern=r"(x|\*)", string=str_area) is None:
        return max(findall(re_area, str_area))
    else:
        tmp = 1
        str_area = sub(r"=\d*", "", str_area)
        for number in findall(pattern=re_area, string=str_area):
            tmp = tmp * float(number)
        
        return tmp

    
def get_area(post_info, attributes):
    if post_info.get_info('attr_area') is not None:
        return float(extract_area(post_info.get_info('attr_area'))) if type(post_info.get_info('attr_area')) is str else float(post_info.get_info('attr_area'))
    else:
        for area in attributes['attr_area']:
            t = extract_area(area)
            if t:
                return float(t)

        # if reaches here, no any string of area extracted by NLP API found
        return 0


def get_interior_floor(post_info, attributes):
    '''Trích xuất thông tin về số tầng của bất động sản
    - Nếu là "tầng", "trệt", "lầu" thì tính là 1
    - Nếu là "lửng", "gác", "sân thượng" thì tính là 0.5
    '''
    if post_info.get_info('attr_interior_floor'):
        return post_info.get_info('attr_interior_floor')

    # trong trường hợp crawl từ Facebook thì "attr_interior_floor" không có sẵn, nên phải trích xuất từ nội dung bài post
    no_floors = 1

    for floor in attributes['attr_interior_floor'].split(' , '):
        floor_fixed = remove_accents(floor)
        tmp = findall(r"\d+", floor_fixed)
        number = int(tmp[0]) if tmp != [] else 0

        # flag này sẽ đánh dấu trong mục interior_floor có chữ "trệt" hay không
        # vấn đề là vì: 
        # - nếu người ta nói "1 trệt 2 tầng/lầu" --> tổng lầu là 3
        # - nếu người ta nói "2 lầu"  --> tổng vẫn là 3
        # - nếu người ta nói "2 tầng"  --> tổng sẽ là 2
        is_ground_available = False
        if search(r"tret", floor_fixed):
            is_ground_available = True
        elif search(r"lau", floor_fixed):
            no_floors += number
        elif search(r"tang", floor_fixed):
            if is_ground_available:
                no_floors += number
            else:
                no_floors += number - 1
        elif search(r"(lung|gac|san thuong)", floor_fixed):
            no_floors += number * 0.5

    return 1 if no_floors == 0 else no_floors