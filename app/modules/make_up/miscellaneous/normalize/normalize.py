from app.modules.make_up.miscellaneous.normalize.utils import *


def normalize(post_info):
    '''Chuẩn hóa các attribute trong instace 'post_info' thuộc class PostInfo
    '''
    post_info.set_info("attr_addr_number",      normalize_address(post_info.get_info("attr_addr_number")))
    post_info.set_info("attr_addr_street",      normalize_street(post_info.get_info("attr_addr_street")))
    post_info.set_info("attr_addr_district",    normalize_district(post_info.get_info("attr_addr_district")))
    post_info.set_info("attr_addr_ward",        normalize_ward(post_info.get_info("attr_addr_ward")))
    post_info.set_info("attr_addr_city",        normalize_city(post_info.get_info("attr_addr_city")))
    post_info.set_info("attr_position",         normalize_position(post_info.get_info("attr_position")))
    post_info.set_info("attr_transaction_type", normalize_transaction_type(post_info.get_info("attr_transaction_type")))
    post_info.set_info("attr_realestate_type",  normalize_realestate_type(post_info.get_info("attr_realestate_type")))
    post_info.set_info("attr_legal",            normalize_legal(post_info.get_info("attr_legal")))
    post_info.set_info("attr_orientation",      normalize_orientation(post_info.get_info("attr_orientation")))
    post_info.set_info("attr_surrounding_name", normalize_surrounding_name(post_info.get_info("attr_surrounding_name")))
    
    


    # chuẩn hóa các attribute rỗng (là các attribute là chuỗi trắng "", mang giá trị 0 hoặc mang giá trị None) về hết giá trị None
    post_info.set_common_value()