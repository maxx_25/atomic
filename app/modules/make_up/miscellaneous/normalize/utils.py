# -*- coding: utf-8 -*-
import re
import requests

import app.modules.make_up.miscellaneous.normalize.convention as convention
from app.modules.make_up.miscellaneous.normalize.Price import Price
from app.modules.make_up.utils import remove_accents


re_addr1 = r"(\d*.*\d*[^\sa-z\W])"
re_addr2 = r"\d{5,8}"                   # search for the mobile number
def normalize_address(str_addr):
    if str_addr is None or str_addr == "" or re.search(re_addr2, str_addr):
        return None

    str_addr = remove_accents(str_addr)
    
    if re.search(re_addr1, str_addr):
        return re.findall(re_addr1, str_addr)[0]
    else:
        return str_addr


def normalize_street(str_street):
    if str_street is None or str_street == "":
        return None

    str_street = re.sub(r"ql",     "quốc lộ",        str_street)
    str_street = re.sub(r"dt|đt",  "đường tỉnh",     str_street)
    str_street = re.sub(r"tl",     "tỉnh lộ",        str_street)
    str_street = re.sub(r"tnhau",  "thoại ngọc hầu", str_street)
    str_street = re.sub(r"hl",     "hương lộ",       str_street)
    str_street = re.sub(r"\d{4,}", "",               str_street)

    return str_street



re_district_1 = r"Q|q\D*(\d{1,2})"
re_district_2 = r"q \. "
def normalize_district(str_district):
    if str_district is None or str_district == "":
        return None

    # tránh trường hợp quốc lộ và tương tự
    if re.search(r"qu.c", str_district):
        return str_district

    if re.search(re_district_1, str_district) and re.search(r"\d+", str_district):
        return "quận " + re.search(r"\d+", str_district).group()

    str_district = re.sub(re_district_2, "", str_district)
    str_district = re.sub(r'pn', "phú nhuận", str_district)
    str_district = re.sub(r'tb', "tân bình", str_district)
    str_district = re.sub(r'gv', "gò vấp", str_district)
    str_district = re.sub(r'bt', "bình thạnh", str_district)
    str_district = re.sub(r'tp', "", str_district)
    str_district = re.sub(r'tx', "", str_district)
    # str_district = re.sub(r'huy.n', "", str_district)

    # thêm chữ "quận" vào tên các quận của Saigon, Hanoi
    # nếu attribute `district` chỉ chứa tên quận (VD: Thủ Đức) mà không chứa các yếu tố khác (VD: chợ Thủ Đức) thì thêm chữ `quận`
    str_district = ("quận " + str_district) if len(re.sub(r"b.nh th.nh|b.nh t.n|g. v.p|th. ..c|t.n ph.|t.n b.nh|ph. nhu.n|g. v.p", "", str_district)) == 0 else str_district

    
    return str_district



re_ward_1 = r"p|P|f|F\D*(\d{1,2})"
re_ward_2 = r"f|p \. "
def normalize_ward(str_ward):
    if str_ward is None or str_ward == "":
        return None

    str_ward = str_ward.lower()
    if re.search(re_ward_1, str_ward) and re.search(r"\d+", str_ward):
        return "phường " + re.search(r"\d+", str_ward).group()

    str_ward = re.sub(re_ward_2, "", str_ward)
    str_ward = re.sub(r"hbp", "hiệp bình phước", str_ward)
    str_ward = re.sub(r"xã\s?", "", str_ward)
    str_ward = re.sub(r"btd", "bình trưng đông", str_ward)



    # sửa trường hợp tên phường chỉ có số, mà không có chữ gì cả (ví dụ "5") thì sẽ
    # tự động thêm chữ "phường" vào trước số đó
    if re.search(r"\d+", str_ward) and re.search(r"\D", str_ward) is None:
        str_ward = "phường " + str_ward

    return str_ward



def normalize_city(str_city):
    '''Return the standardized name of the city
    In this version, it returns the alias names of the city
    '''
    if str_city is None or str_city == "":
        return None
    
    str_city = re.sub(r"tp [\. ]?", "", str_city).lower()
    tmp = str_city
    str_city = remove_accents(str_city)
    
    
    for tag, value in convention.CITIES.items():
        for alias in value['alias']:
            if re.search(alias, str_city):
                return tag
    return tmp


def normalize_position(str_position):
    if str_position is None or str_position == "":
        return None

    str_position = remove_accents(str_position)
    if re.search(r"mat|mt", str_position):
        return "mặt tiền"
    elif re.search(r"hem|ngo|hxh", str_position):
        return "hẻm"
    else:
        return "khác"


def normalize_transaction_type(list_transaction_type):
    '''Return the standardized transaction type
    In this version, it returns the alias names of the transaction
    '''
    if list_transaction_type == []:
        return None
    if type(list_transaction_type) is str:
        list_transaction_type = [list_transaction_type]

    for str_transaction_type, i in zip(list_transaction_type, range(len(list_transaction_type))):
        is_found_flag = False
        str_transaction_type = remove_accents(str_transaction_type)

        for tag, value in convention.TRANSACTION_TYPE.items():
            for alias in value['aliases']:
                if re.search(alias, str_transaction_type):
                    list_transaction_type[i] = tag
                    is_found_flag =  True
            
            if is_found_flag == False:
                list_transaction_type[i] = "khác"

    # eliminate 2 same transaction_types
    result = []
    for transaction_type in list_transaction_type:
        if transaction_type not in result:
            result.append(transaction_type)

    return ','.join(result)


def normalize_realestate_type(str_realestate_type):
    '''Return the standardized real estate type
    In this version, it returns the alias names of the real estate
    '''
    if str_realestate_type is None or str_realestate_type == "":
        return None

    tmp = str_realestate_type
    str_realestate_type = remove_accents(str_realestate_type)

    for tag, value in convention.REALESTATE_TYPE.items():
        for alias in value['aliases']:
            if re.search(alias, str_realestate_type):
                return tag

    return "khác"


def normalize_legal(str_legal):
    if str_legal is None or str_legal == "":
        return None

    str_legal = remove_accents(str_legal)

    if re.search(r"hong|sh", str_legal):
        return "sổ hồng"
    elif re.search(r"do|sd", str_legal):
        return "sổ đỏ"
    else:
        return "khác"


def normalize_price(str_price):
    '''Extract price_min, price_max
       May detect that price is of price per meter square or price of the whole real estate
       May detect area 
    
    :Args:
    str_price - string of price extracted by NLP API

    :Returns:
    a 5-element tuple of: (price_min, price_max, price_min_m2, price_min_m2, area), None value may be one of 5 elements in tuple
    '''

    # remove phone number if exists
    str_price = re.sub(r"(0\d{7,10})", "", str_price)

    # basic processing
    str_price = remove_accents(str_price)

    str_price = re.sub('Mười','10', str_price)
    str_price = re.sub('mười','10', str_price)
    str_price = re.sub('tỏi', 'ty', str_price)

    str_price = remove_accents(str_price)
    
    for number, spelling in convention.NUMBER.items():
        str_price = re.sub(r"\b{}\b".format(spelling), number, str_price)

    # subtitute unconventional spelling name to conventional one
    for conventional, value in convention.NUMBER_CARDINALITY.items():
        for alias in value['aliases']:

            # this command for the case: 33t / m2
            if (alias == 't'or alias == 'tt') and re.search(r"m|m2", str_price):
                str_price = re.sub(r"\b({})\b".format(alias), "trieu", str_price)
            else:
                str_price = re.sub(r"\b({})\b".format(alias), conventional, str_price)

    # remove space
    str_price = re.sub(r'\s','', str_price)
    
   
    # recognize m2, usd
    # we have to check the following condition because in some case the price is "570 tr / 1100 m 2"
    # this price is for the whole 1100-meter-square land, not per meter square
    is_price_m2 = False
    is_usd = False
    area = re.search(r"\d{2,}met|\d{2,}m2|\d{2,}m", str_price)
    if area is None:
        if re.search(r"1metvuong|metvuong|met|m2|m|\d+lo|\d+can", str_price):
            str_price = re.sub(r"1metvuong|metvuong|met|m2|m|\d+lo|\d+can", "", str_price)
            is_price_m2 = True
    else:
        # the price has the form ""800 tr / 191 m 2"
        area = float(re.sub(r"metvuong|met|m2|m", "", area.group()))
        str_price = re.sub(r"\d{2,}met|\d{2,}m2|\d{2,}m", "", str_price)

    for alias in convention.FOREIGN_CURRENCY['usd']:
        if re.search(alias, str_price):
            is_usd = True
            break
    
    
    
    
    
    # split str_price into 2 parts
    for divider in convention.DIVIDERS:
        str_price = re.sub(r"{}".format(divider), convention.MAIN_DIVIDER, str_price)

    str_price_parts = []
    for part in str_price.split(convention.MAIN_DIVIDER):
        if part != '':
            str_price_parts.append(Price(part, is_price_m2, is_usd))

    # each part recognizes itself
    for i in range(len(str_price_parts)):        
        str_price_parts[i].recognize()


    # if one of part are price_m2 or dollar or has biggest cardinality available, set it to the rest
    if len(str_price_parts) > 1:
        # there are 2 parts of price

        # for cardinality, it is a little bit different
        #  if 2 parts have their own biggest cardinality, ignore this case
        #  if part 2 has while part 1 doesn't, there are 2 cases:
        #    - 2 parts share the same cardinality
        #    - part 1 has lower cardinality
        
        # below only solves first case given, for the second case, it will be solve after calculating value for each part
        if str_price_parts[0].get_biggest_cardinality() is None:
            str_price_parts[0].set_biggest_cardinality(str_price_parts[1].get_biggest_cardinality())

    
    # calculate price and print out
    for i in range(len(str_price_parts)):
        str_price_parts[i].calculate_price()
        # str_price_parts[i].debug()
        
        
    
    
    # return results
    if len(str_price_parts) == 0:
        return None, None, None, None, None

    
    price_min    = str_price_parts[0].get_price()
    price_min_m2 = str_price_parts[0].get_price_m2()
    if len(str_price_parts) > 1:
        price_max    = str_price_parts[1].get_price()
        price_max_m2 = str_price_parts[1].get_price_m2()
    else:
        price_max = price_max_m2 = None

    # solve the second case of the problem of missing cardinality of first part
    if price_max and price_min:
        while price_min > price_max:
            price_min = price_min / 1000

    return price_min, price_max, price_min_m2, price_max_m2, area



def normalize_orientation(str_orientation):
    if str_orientation is None or str_orientation == "":
        return None

    if str_orientation == "đn":
        return "đông - nam"
    if str_orientation == "đb":
        return "đông - bắc"
    if str_orientation == "tn":
        return "tây - nam"
    if str_orientation == "tb":
        return "tây - bắc"

    return None


def normalize_surrounding_name(surrounding_long_name):
    if surrounding_long_name == "" or surrounding_long_name is None:
        return None

    full_name_list = []
    surrounding_name_list = surrounding_long_name.split(' , ')
    for surrounding_name in surrounding_name_list:
        if surrounding_name == "":
            continue
        
        # thay thế một vài từ viết tắt bằng từ hợp lí
        for short_name, full_name in convention.SURROUNDING_NAME.items():
            surrounding_name = surrounding_name.replace(short_name, full_name)

        # đưa qua xử lí normalize_district và normalize_ward trước
        tmp = normalize_district(surrounding_name)
        # tmp = normalize_ward(tmp)

        tmp = tmp.replace("  ", " ")

        full_name_list.append(tmp)


    if full_name_list is None:
        return None
    else:
        return_list = []
        
        for name in full_name_list:
            # chuẩn hoá `name` chút xíu
            # với trường hợp 'uỷ ban nhân dân phường, quận, thì nếu có chữ quận, phường ở dạng viết tắt thì ghi tường minh ra chữ 'quận', 'phường'
            name = name.replace('.', '')
            name = name.replace(',', '')

            if re.search(r"ph..ng|qu.n", name) is None:                        
                tmp = re.findall(r".. ban nh.n d.n p(.*)", name)
                if len(tmp) > 0:
                    name = "uỷ ban nhân dân phường" + tmp[0]
                tmp = re.findall(r".. ban nh.n d.n q(.*)", name)
                if len(tmp) > 0:
                    name = "uỷ ban nhân dân quận" + tmp[0]

            return_list.append(name)
            
        return ", ".join(return_list)