from time import sleep
import requests
import logging


import app.settings as settings
    


def get_from_ggmap(address):
    '''Lấy lat, long từ Google API
    '''
    if settings.ENV == "DEV":
        return None
    elif settings.ENV != "PROD":
        logging.error("Bad ENV")
        exit(1)

    # Do the request and get the response data
    sleep(0.75)
    # settings.LOGGING.debug("Access Google API with location: " + address)
    # print("Access Google API with location: " + address)

    params = {
        'address': address,
        'componentRestrictions': {
            'country': 'VN'
        }
    }
    req = requests.get(settings.LINK_GOOGLEAPI + settings.KEY_GOOGLEAPI, params=params)
    res = req.json()
    # Use the first result
    try:
        return res['results'][0]['geometry']['location']
    except IndexError:
        return None


# if __name__ == "__main__":
#     print(get_from_ggmap("quận 1"))
