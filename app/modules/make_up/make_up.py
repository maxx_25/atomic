from app.modules.make_up.miscellaneous.get_attribute.get_attribute import get_attribute
from app.modules.make_up.miscellaneous.normalize.normalize import normalize
from app.modules.make_up.miscellaneous.improve.improve import improve
import app.settings


def make_up(post_info):
    '''Make up dữ liệu của bài post
    '''

    # print("====================== MAKE UP DATA =========================")

    # thay thế kí tự '₫' bởi chuỗi " giá "
    post_info.set_info("message", post_info.get_info("message").replace('₫', " giá "))


    ##################################################################################################
    ########################################## TRÍCH XUẤT ############################################
    ##################################################################################################
    # Các attribute của bài post khá lung tung nên sẽ rất khó để tìm kiếm chúng khi đưa lên DB, 
    # một loạt các câu lệnh tiếp theo sẽ chuẩn hóa chúng để tiện cho việc lưu trữ trên Mongo cũng như các bước xử lí tiếp theo
    get_attribute(post_info)  


    ##################################################################################################
    ########################################### CHUẨN HÓA ############################################
    ##################################################################################################
    # Các attribute của bài post khá lung tung nên sẽ rất khó để tìm kiếm chúng khi đưa lên DB, 
    # một loạt các câu lệnh tiếp theo sẽ chuẩn hóa chúng để tiện cho việc lưu trữ trên Mongo cũng như các bước xử lí tiếp theo
    normalize(post_info)   


    ##################################################################################################
    #################################### CẢI THIỆN THUỘC TÍNH  #######################################
    ##################################################################################################
    # Một số attribute có vẻ sai hoặc chưa hợp lí, bước này sẽ đảm bảo dữ liệu được gửi lên DB ít sai sót nhất có thể
    improve(post_info)