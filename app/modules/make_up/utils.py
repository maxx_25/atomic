# -*- coding: utf-8 -*-
''' Tập hợp các hàm được dùng chung trong toàn bộ các hàm thuộc nhóm `make_up`
'''

from pymongo import MongoClient

import app.settings as settings

s1 = "ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚÝàáâãèéêìíòóôõùúýĂăĐđĨĩŨũƠơƯưẠạẢảẤấẦầẨẩẪẫẬậẮắẰằẲẳẴẵẶặẸẹẺẻẼẽẾếỀềỂểỄễỆệỈỉỊịỌọỎỏỐốỒồỔổỖỗỘộỚớỜờỞởỠỡỢợỤụỦủỨứỪừỬửỮữỰựỲỳỴỵỶỷỸỹ"
s0 = "AAAAEEEIIOOOOUUYaaaaeeeiioooouuyAaDdIiUuOoUuAaAaAaAaAaAaAaAaAaAaAaAaEeEeEeEeEeEeEeEeIiIiOoOoOoOoOoOoOoOoOoOoOoOoUuUuUuUuUuUuUuYyYyYyYy"
def remove_accents(input_str):
    '''Đổi các kí tự Unicode sang 
    '''
    s = ""
    for c in input_str:
        if c in s1:
            s += s0[s1.index(c)]
        else:
            s += c
    return s.lower()




def send_to_dbGeoErr(location, addr_street, addr_district, addr_ward, addr_city):
    '''Gửi lên DB chứa các địa danh cần con người kiểm tra
    :Args:
    - location - tên của địa danh (đã qua xử lí)
    - addr_street, addr_district, addr_ward, addr_city - các thuộc tính của địa danh, những field này có thể có hoặc không
    :Rets:
    void
    '''

    if location is None:
        return

    client = MongoClient(settings.MONGO_LINK)
    errGeo_coll = client[settings.MONGO_DB][settings.MONGO_COLL_ERRGEO]
    errGeo_coll.insert_one({
        "location"              : location,
        "standardized_location" : None,
        "add_street"            : addr_street,
        "add_district"          : addr_district,
        "add_ward"              : addr_ward,
        "add_city"              : addr_city,
        'location_lat'          : None,
        'location_lng'          : None,
    })
    client.close()


def send_to_dbGeo(location, addr_street, addr_district, addr_ward, addr_city, location_lat, location_lng):
    '''Gửi lên DB chứa các địa danh đã xác định lat, lng hợp lệ
    :Args:
    - location - tên của địa danh (đã qua xử lí)
    - addr_street, addr_district, addr_ward, addr_city - các thuộc tính của địa danh, những field này có thể có hoặc không
    :Rets:
    void
    '''

    if location is None:
        return

    client = MongoClient(settings.MONGO_LINK)
    errGeo_coll = client[settings.MONGO_DB][settings.MONGO_COLL_GEO]
    errGeo_coll.insert_one({
        "location"              : location,
        "standardized_location" : None,
        "add_street"            : addr_street,
        "add_district"          : addr_district,
        "add_ward"              : addr_ward,
        "add_city"              : addr_city,
        'location_lat'          : location_lat,
        'location_lng'          : location_lng,
    })
    client.close()


def find_geolocation_by_name(location, addr_district, addr_city):
    '''Tìm tên địa danh trên DB Geolocation
    Cơ chế tìm là so trùng chuỗi

    :Args:
    - location - tên của địa danh
    - addr_district, addr_city - tên quận/huyện và thành phố/tỉnh mà địa danh toạ lạc
    :Rets:
    - (lat, long, addr_street, addr_district, addr_ward, addr_city) của tên địa danh nếu tìm thấy
    - None trong trường hợp còn lại
    '''

    if type(location) != str:
        return None

    client = MongoClient(settings.MONGO_LINK)
    geo_collection = client[settings.MONGO_DB][settings.MONGO_COLL_GEO]
    # nếu
    results = geo_collection.find({'location' : location})
    client.close()

    if results:
        # 2 list dưới đây sẽ chứa các địa danh 
        list_3_atr_matched = []
        list_2_atr_matched = []

        for geolocation in results:
            # nếu trong danh sách có 'địa danh duy nhất' thì chọn địa danh đó ngay
            # và hàm 'try' này sẽ kiểm tra, nếu có key 'isUnique' trong dict geolocation thì không bị catch KeyError
            try :
                geolocation['isUnique'] = True

                return geolocation['location_lat'], geolocation['location_lng'], \
                        geolocation['add_street'], geolocation['add_district'], geolocation['add_ward'], geolocation['add_city']
            except KeyError:
                # geolocation này không phải duy nhất, nên tìm xem trong các geolocation trả về có cái nào trùng tên quận/huyện và (hoặc) tên thành phố không
                if addr_district and geolocation['add_district'] == addr_district:
                    if addr_city and geolocation['add_city'] == addr_city:
                        list_3_atr_matched.append(geolocation)
                    else:
                        list_2_atr_matched.append(geolocation)
                if addr_city and geolocation['add_city'] == addr_city:
                    list_2_atr_matched.append(geolocation)

        if len(list_3_atr_matched) > 0:
            geolocation = list_3_atr_matched[0]
            return geolocation['location_lat'], geolocation['location_lng'], \
                    geolocation['add_street'], geolocation['add_district'], geolocation['add_ward'], geolocation['add_city']
        elif len(list_2_atr_matched) > 0:
            geolocation = list_2_atr_matched[0]
            return geolocation['location_lat'], geolocation['location_lng'], \
                    geolocation['add_street'], geolocation['add_district'], geolocation['add_ward'], geolocation['add_city']                
    
    return None