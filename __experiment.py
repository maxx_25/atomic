# -*- coding: utf8 -*-
import math
from pandas import DataFrame
from app.modules.make_up.api.api_GGMap_communicate import get_from_ggmap
from app.modules.make_up.miscellaneous.improve.LatLng import LatLng
from app.modules.make_up.miscellaneous.improve.improve import fill_lat_long
from app.modules.make_up.miscellaneous.improve.improve import checklocation
from app.modules.make_up.miscellaneous.improve.improve import distance
from app.modules.make_up.api.api_NLP_communicate import get_from_api
from app.modules.PostInfo import PostInfo
from math import pi,sqrt,sin,cos,atan2
from bson import ObjectId
import time
### HƯỚNG DẪN ANH CÁCH CHẠY ###
# Vì khi chạy 1 file, Python sẽ bắt đầu từ folder chứa file đó để tìm các thư viện mà import
# nên nếu muốn chạy độc lập các module trong từng file, mình chỉ có cách là gọi module muốn chạy
# ở file nằm tại root directory của project thì mới tránh rắc rối

# Như anh thấy để chạy hàm trong file em phải import hàm đó ở ngoài đây



def main():
    # Đây là đoạn code dùng để gọi hàm tính lat, lng, tương ứng với mục 4.1.4.2, mục 1. Quyết định sẽ lấy những lat, lng nào để tính toán
    # hàm fill_lat_long() có argument là instance của class PostInfo, nên để chạy được hàm mình cần tạo một instance của class PostInfo
    # post_info = PostInfo()
    
    data = {"_id" : ObjectId("5d47b5a592832e04b1313768"),
    "page" : "https://facebook.com/",
    "link" : "https://facebook.com/groups/698275343680722/permalink/1253994941442090",
    "title" : None,
    "post_id" : "1253994941442090",
    "message" : "   💙💙💙 BÁN NHÀ GẦN CHỢ CẦU GÒ VẤP , HẺM XE TẢI(6M) 💙💙💙\n❤️ Vị trí: Đường Pham Văn Chiêu, Phường 8, Gò Vấp\n❤️ Diện tích 4x15m.\n❤️ Hướng: Tây Nam\n❤️ Đúc trệt lầu 02 phòng ngủ. Nhà thiết kế hiện đại.\n❤️ Gần chợ , trường học, chùa..\n❤️ Giá bán 4ty 400 triệu ( bớt lộc)\n☎️☎️☎️ Alo/Zalo : Hà - 0398781490  ",
    "post_date" : "2019-07-09T09:31:00",
    "crawled_date" : "2019-07-09T10:23:55.201906",
    "score" : 0,
    "attr_addr_number" : None,
    "attr_addr_street" : "pham văn chiêu",
    "attr_addr_district" : "gò vấp",
    "attr_addr_ward" : "phường 8",
    "attr_addr_city" : "hồ chí minh",
    "attr_position" : "hẻm",
    "attr_surrounding" : "chợ , cầu , chợ , trường học , chùa",
    "attr_surrounding_name" : "cầu gò vấp",
    "attr_surrounding_characteristics" : "None",
    "attr_transaction_type" : "bán",
    "attr_realestate_type" : "nhà",
    "attr_potential" : None,
    "attr_area" : 60.0,
    "attr_price" : "4 ty 400 triệu",
    "attr_price_min" : 4400000000.0,
    "attr_price_max" : 0,
    "attr_price_m2" : 73333333.3333333,
    "attr_interior_floor" : "trệt , lầu",
    "attr_interior_room" : "02 phòng ngủ",
    "attr_orientation" : "tây nam",
    "attr_project" : None,
    "attr_legal" : None,
    "location_lng" : 106.6544563,
    "location_lat" : 10.8508477,
    "hash_val" : "5759c9bf87236599d17469a10b64ef7c4ca2efd83a91598772389d98efded0d2",
    "location" : {
        "type" : "Point",
        "coordinates" : [ 
            106.6544563, 
            10.8508477
        ]
    }
}
    start_time = time.time()
    checklocation(data)
    end_time = time.time()
    print ('total run-time: ', ((end_time - start_time)))
        # Tại thời điểm viết code của hàm này, chưa đề xuất việc sẽ căn cứ vào surrounding_name và địa chỉ như thế nào mà chỉ lấy trung bình của tất các chúng nó
        # (trung bình của các surrounding và địa chỉ, dòng 394, 429 và 435 trong file improve.py).
        # Khi các anh hoàn thành code của phần 4.1.4.2, mục 2, tức là hàm calculate() trong class LatLng ở file LatLng.py, thì lúc đó các anh thay hàm get_ave_lat_lng()
        # ở các dòng 394, 429 và 435 bằng hàm calculate()

    # post_info.set_info("location_lat",10.8248725)
    # post_info.set_info("location_lng",106.6796234)

    # post_info.set_info("location_lat",10.838543)
    # post_info.set_info("location_lng", 106.675472)

    # fill_lat_long(post_info)

    # print(post_info.get_info('location_lat'))
    # print(post_info.get_info('location_lng'))

        # Đây là đoạn code dùng để gọi hàm tính lat, lng, tương ứng với mục 4.1.4.2, mục 2.Tính toán lat, lng của bất động sản dựa vào các lat, lng đã thu thập được
        # Em thiết kế class LatLng chỉ cần sử dụng các hàm sau:
        # - add_surrounding_point(): thêm lat, lng của surrounding, có bao nhiêu surrounding có lat, lng thì gọi bấy nhiêu hàm này
        # - add_address_point(): thêm lat, lng của địa chỉ. Vì địa chỉ chỉ có 1 nên chỉ cần gọi 1 lần hàm này
        # - calculate(): tính toán lat, lng dựa vào các lat, lng đã thêm trước đó
    
    # lat_lng.add_surrounding_point(10.729592, 106.721631,0)
    # lat_lng.add_surrounding_point(10.732502, 106.718087,0)
    # lat_lng.add_surrounding_point(10.802992, 106.731785,0)
    # with open('addr_nghiencuu.txt',encoding="utf8") as file:
    #     for line in file:
    # print(get_from_ggmap("coopmart bình triệu"))
   
    # lat_lng.add_surrounding_point(10.772693, 106.697902, 0)
    # lat_lng.add_surrounding_point(10.770991, 106.694827, 0)

    # lat_lng.add_address_point(0, 0)

    
    # print(lat_lng.calculate())

    # kết quả của hàm calculate() chính là lat, lng cuối cùng của bất động sản, mình sẽ gửi lat, lng này lên DB
    # chỉnh sửa thuật toán tính lat, lng dựa vào anchor thì chỉnh trong hàm calculate()
    # print(lat_lng.find_representing_point(10.883928, 106.586813, 10.917237, 106.624815, lat_lng.convert(3000)))

if __name__ == "__main__":
    main()

#calculate tính toán tọa độ của bđs
#find_representing_point: độ dài của đi
# distance: tinh khoang cach giua 2 diem them vao file improve.py
# checklocation: ham tinh lai lat,lng cua bds