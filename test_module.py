  # coding=utf8 

from app.modules.make_up.make_up import make_up
from app.modules.PostInfo import PostInfo


msg = "CHO THUÊ PHÒNG CAO CẤP ( TRONG 1 CĂN NHÀ LẦU, GỒM CÓ 2 LẦU ), ĐƯỜNG BẠCH ĐẰNG, PHƯỜNG 24, QUẬN BÌNH THẠNH ( Giá từ 4,8 Triệu - 5 Triệu ) *** Nằm ở mặt tiền hẻm ( Thuộc đường Bạch Đằng, Gần ngã tư Bạch Đằng/ Đinh Bộ Lĩnh )\n- Hẻm rộng thênh thang, xe hơi có thể quay đầu. Khu vực an ninh, yên tĩnh..\n- Phòng nằm trong căn nhà lầu gồm có 2 lầu xây lệch tầng. Có 6 Phòng, mỗi Phòng DT từ ( 15 - 18m² ) - Giá từ ( 4,8 - 5 Triệu ) :\n• 2 PHÒNG LỚN : ( 18m² - 5 Triệu - Có Ban công )\n• 4 PHÒNG NHỎ : ( 15m² - 4,8 Triệu - Không Ban công )\n* NẾU AI THUÊ HẾT NGUYÊN CĂN : ( 1 TRỆT 2 LẦU, thì sẽ có thêm 1 Phòng ngủ, 1 Phòng khách, 1 Mặt bằng ở dưới tầng trệt và Sân thượng ở bên trên ) : GIÁ RẺ BẤT NGỜ : ( 35 Triệu/ Nguyên căn )\n- Phòng ở được khoảng từ 2 - 3 Người\n- Phòng nào cũng có cửa sổ đầy đủ.\n- Lối đi chung, nhưng không chung chủ.\n- Phòng sạch đẹp, thoáng mát, đầy đủ tiện nghi ( Mỗi Phòng đều có : Máy lạnh, Máy giặt, Tủ lạnh, Nệm, Máy nóng lạnh, Tủ quần áo, Bàn ghế, Toilet trong phòng.)\n* ĐIỆN : ( 3,5 K/ 1 KW )\n* NƯỚC : ( 100 K/ Người/ Tháng )\n* PHÍ : Vệ sinh + Rác + Máy giặt + Wifi ( 100K/ 1 Phòng/ 1 Tháng )\n- Xe được để 2 chiếc/ 1 Phòng ( Nếu dư thì phải gửi bên ngoài )\n• CÓ KHU SINH HOẠT CHUNG ( Có người làm vệ sinh Cầu thang và Khu sinh hoạt chung )\n• CÓ 2 PHÒNG BẾP :\n- 1/ Ở Tầng trệt. - 2/ Ở Sân thượng\n( Có trang bị Bếp Hồng ngoại, Sử dụng chung )\n- Nằm ngay gần Trung Tâm TP, Gần các Công ty, Các Trường ĐH, Trường PT Cấp 1,2,3, Nhà trẻ, Mẫu giáo.. Các Chợ, Tiệm tạp hóa, Chùa, Nhà Thờ, Ngân hàng, Bệnh viện, Nha khoa, Thẩm mỹ viện, Công viên, Hồ bơi, Sở thú, Khu ăn uống vui chơi giải trí, Thức ăn nhanh như Gà rán KFC, Pizza.. Gần các Câu lạc bộ TDTT, Thể hình, TD Thẩm mỹ, Khiêu vũ, Yoga.. Gần các Nhà Văn hóa lớn, Nhà sách, Thế Giới di động, Điện máy và các Trung Tâm mua sắm lớn.. Và còn nhiều tiện ích khác nữa.. Nói chung là rất thuận tiện cho việc đi lại, làm ăn, buôn bán, học tập của mọi người khắp mọi nơi..! *** Nếu ai có nhu cầu muốn thuê, xin liên hệ trực tiếp với ( C. Vân ) qua số điện ( 0368601388 ), để hẹn đi xem nhà. Mong được tiếp khách có thiện chí, để tránh làm mất thời gian của đôi bên..!"

post_info = PostInfo()
post_info.set_info('message', msg)
# post_info.set_info('attr_addr_street', "hưng lợi, ninh kiều, cần thơ")
# post_info.set_info('attr_addr_district', "ninh kiều")
# post_info.set_info('attr_addr_city', "cần thơ")
# post_info.set_info('attr_orientation', "tây - nam")
# post_info.set_info('attr_transaction_type', "bán")
# post_info.set_info('attr_realestate_type', "nhà")
# post_info.set_info('attr_area', "50")
# post_info.set_info('attr_price', "2.48 tỷ")
# post_info.set_info('attr_interior_floor', 1)
# post_info.set_info('attr_interior_room', "2 phòng ngủ")
# post_info.set_info('attr_legal', "sổ hồng")
# post_info.set_info('location_lat', 10.01868655)
# post_info.set_info('location_lng', 105.7690946)
# post_info.set_info("location_lat", 10.7956226)
# post_info.set_info("location_lng", 106.640833)

make_up(post_info)

for attribute, value in post_info.get_info_all().items():
    print(attribute, " : ", value)
